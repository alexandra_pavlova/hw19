 
#include <iostream>

using namespace std;

class Animal
{
private:
    int x;
public:
    Animal() {}
    virtual void Voice()
    {
        cout << "*Jungle noizes*" << '\n';
    }
};

class Jaguar : public Animal
{
private:
    int y;
public:
    Jaguar() {}
    void Voice() override
    {
        cout << "Arrrrrr!\n";
    }
};

class Monkey : public Animal
{
private:
    int z;
public:
    Monkey() {}
    void Voice() override
    {
        cout << "Oiiiiii!\n";
    }
};

class Anaconda :public Animal
{
private:
    int m;
public:
    Anaconda() {}
    void Voice() override
    {
        cout << "Hssssss!\n";
    }
};

int main()
{
    // ���������� �������
    Animal* p[9];
    for (int i = 0; i < 9; i++)
    {
        if (i == 0)
        {
            p[i] = new Animal();
            p[i]->Voice();
        };
        if ((i % 3 == 0)&(i!=0))
        {
            p[i] = new Jaguar();
            p[i]->Voice();
        };
        if (i % 3 == 1)
        {
            p[i] = new Monkey();
            p[i]->Voice();
        };
        if (i % 3 == 2)
        {
            p[i] = new Anaconda();
            p[i]->Voice();
        }
    }
    cout << '\n';
    // ��������� �������
    Animal* k[9]{ new Monkey, new Anaconda, new Jaguar,
                  new Animal, new Jaguar, new Monkey, new Monkey,
                  new Animal, new Jaguar };
    for (int j = 0; j < 9; j++)
        k[j]->Voice();
    return 0;
}